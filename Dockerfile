FROM golang:latest
ENV GO111MODULE on
ADD ./* /project/
WORKDIR /project/
RUN go mod init "gitlab-cicd"
RUN go build ./main.go
CMD ./main
