package main
import (
	"testing"
	"github.com/stretchr/testify/assert"
)
// Simple testing what different between Fatal and Error
func TestNew(t *testing.T) {
	c, err := New("", "Taiwan")
	if err != nil {
		t.Fatal("got errors:", err)
	}
	if c == nil {
		t.Error("User should be nil")
	}
}

// Simple testing with testify tool
func TestNewWithAssert(t *testing.T) {
	c, err := New("", "Taiwan")
	assert.NotNil(t, err)
	assert.Error(t, err)
	assert.Nil(t, c)

	c, err = New("Shawn", "Taiwan")
	assert.Nil(t, err)
	assert.NoError(t, err)
	assert.NotNil(t, c)
	assert.Equal(t, "Shawn", c.Name)
}
